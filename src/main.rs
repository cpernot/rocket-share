#[macro_use] extern crate rocket;
use std::path::PathBuf;
#[launch]
fn rocket() -> _ {
    let path = PathBuf::from(std::env::var("HOME").unwrap_or_default()).join("share");
    rocket::build().mount("/",rocket::fs::FileServer::from(path))
}
